
-- DDL TABLAS
CREATE TABLE IF NOT EXISTS TipoContacto(
  idTipoContacto INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
  NombreTipoContacto TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS domicilio (
  idDomicilio INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
  Calle TEXT NOT NULL, Altura TEXT NOT NULL, 
  Piso TEXT NOT NULL, Depto TEXT NOT NULL, 
  idLocalidad int NOT NULL, idProvincia int NOT NULL, 
  idPais int NOT NULL
);

CREATE TABLE IF NOT EXISTS personas (
  idPersona INT PRIMARY KEY NOT NULL, Nombre TEXT NOT NULL, 
  Apellido TEXT NOT NULL, Telefono TEXT NOT NULL, 
  Email TEXT NOT NULL, idDomicilio INT NOT NULL, 
  Nacimiento TEXT NOT NULL, idTipoContacto INT NOT NULL,
  UsuarioLinkedin TEXT NOT NULL, MesNacimiento INT NOT NULL
);

CREATE TABLE IF NOT EXISTS provincia (
  idPronvincia INT PRIMARY KEY NOT NULL, 
  idPais INT NOT NULL, nombre TEXT NOT NULL
);

 CREATE TABLE IF NOT EXISTS localidad (
  idLocalidad INT PRIMARY KEY NOT NULL, 
  idProvincia INT NOT NULL, idPais INT NOT NULL, 
  nombre TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS pais (
  idPais INT PRIMARY KEY NOT NULL, nombre TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS pais (
  idPais INT PRIMARY KEY NOT NULL, nombre TEXT NOT NULL
);


CREATE TABLE IF NOT EXISTS pais (
  idPais INT PRIMARY KEY NOT NULL, nombre TEXT NOT NULL
);



INSERT INTO pais(idPais, nombre) 
VALUES 
  (1, "Argentina");
  
INSERT INTO pais(idPais, nombre) 
VALUES 
  (2, "Uruguay");
  
  
INSERT INTO provincia(idPronvincia, idPais, nombre) 
VALUES 
  (1, 1, "Buenos Aires");
  
INSERT INTO provincia(idPronvincia, idPais, nombre) 
VALUES 
  (2, 1, "Cordoba");
  
  
INSERT INTO provincia(idPronvincia, idPais, nombre) 
VALUES 
  (3, 2, "Florida");
  
INSERT INTO provincia(idPronvincia, idPais, nombre) 
VALUES 
  (4, 2, "Colonia");
  
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
)
VALUES 
  (1, 1, 1, "Pilar");
  
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (2, 1, 1, "Polvorines");
  
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (3, 2, 1, "Tanti");
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (4, 2, 1, "Valle Hermoso");
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (5, 3, 2, "Cerro chato");
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (6, 3, 2, "Bellondo");
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (7, 4, 2, "Carmelo");
INSERT INTO localidad(
  idLocalidad, idProvincia, idPais, 
  nombre
) VALUES 
  (8, 4, 2, "Nueva palmira");
INSERT INTO TipoContacto(
  idTipoContacto, NombreTipoContacto
) VALUES 
  (1, "Familia");
INSERT INTO TipoContacto(
  idTipoContacto, NombreTipoContacto
) VALUES 
  (2, "Amigos");
INSERT INTO TipoContacto(
  idTipoContacto, NombreTipoContacto
)VALUES 
  (3, "Trabajo");
INSERT INTO TipoContacto(
  idTipoContacto, NombreTipoContacto
)VALUES 
  (4, "Servicios");
INSERT INTO TipoContacto(
  idTipoContacto, NombreTipoContacto
) VALUES 
  (5, "Otros");

