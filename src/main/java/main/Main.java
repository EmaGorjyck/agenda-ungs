package main;

import java.io.IOException;

import modelo.Agenda;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.LoginController;
import presentacion.vista.Login;


public class Main 
{

	public static void main(String[] args) throws IOException 
	{
//		Login vista = new Login();
//		Vista vista = new Vista();
//		Agenda modelo = new Agenda(new DAOSQLFactory());
//		Controlador controlador = new Controlador(vista, modelo);
		LoginController controlador = new LoginController();

		controlador.inicializar();
	}
}
