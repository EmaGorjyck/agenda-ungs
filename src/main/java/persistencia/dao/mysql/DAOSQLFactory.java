/**
 * 
 */
package persistencia.dao.mysql;

import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;

public class DAOSQLFactory implements DAOAbstractFactory 
{
	/* (non-Javadoc)
	 * @see persistencia.dao.interfaz.DAOAbstractFactory#createPersonaDAO()
	 */
	public PersonaDAO createPersonaDAO() 
	{
				return new PersonaDAOSQL();
	}
	
	public TipoContactoDAO createContactoDAO() 
	{
				return new TipoContactoDAOSQL();
	}
	
	public LocalidadDAO createLocalidadDAO() 
	{
				return new LocalidadDAOSQL();
	}

	@Override
	public ProvinciaDAO createProvinciaDAO() {
		// TODO Auto-generated method stub
		return new ProvinciaDAOSQL();
	}

	@Override
	public PaisDAO createPaisDAO() {
		// TODO Auto-generated method stub
		return new PaisDAOSQL();
	}

	@Override
	public DomicilioDAO createDomicilioDAO() {
		// TODO Auto-generated method stub
		return new DomicilioDAOSQL();
	}

	@Override
	public TipoContactoDAO createTipoContactoDAO() {
		// TODO Auto-generated method stub
		return new TipoContactoDAOSQL();
	}
	

}
