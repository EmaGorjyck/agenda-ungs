package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, apellido, telefono, email, idDomicilio, nacimiento, idTipoContacto, UsuarioLinkedin, MesNacimiento) VALUES(?, ?, ?,?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";

	private static final String update = "UPDATE personas SET nombre = ?, apellido = ?, telefono = ?, email = ?, idDomicilio = ?, nacimiento = ?, idTipoContacto = ?, UsuarioLinkedin = ?, MesNacimiento = ? WHERE idPersona =?";


	private static final String readall = "SELECT * FROM personas";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getApellido());
			statement.setString(4, persona.getTelefono());
			statement.setString(5, persona.getDireccionEmail());
			statement.setInt(6, persona.getDomicilio());
			statement.setString(7, persona.getFechaCumple());
			statement.setInt(8, persona.getTipoContacto());
			statement.setString(9, persona.getUsuarioLinkedin());
			statement.setString(10, persona.getMesNacimiento());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	public boolean update(PersonaDTO persona_a_actualizar){
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			statement = conexion.prepareStatement(update);
			statement.setString(1, persona_a_actualizar.getNombre());
			statement.setString(2, persona_a_actualizar.getApellido());
			statement.setString(3, persona_a_actualizar.getTelefono());
			statement.setString(4, persona_a_actualizar.getDireccionEmail());

			statement.setInt(5, persona_a_actualizar.getDomicilio());
			statement.setString(6, persona_a_actualizar.getFechaCumple());
			statement.setInt(7, persona_a_actualizar.getTipoContacto());
			statement.setString(8, persona_a_actualizar.getUsuarioLinkedin());
			statement.setString(9, persona_a_actualizar.getMesNacimiento());
			statement.setInt(10, persona_a_actualizar.getIdPersona());

			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isUpdateExitoso = true;
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}

		return isUpdateExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall + " ORDER by nombre COLLATE NOCASE");
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String apellido = resultSet.getString("Apellido");
		String tel = resultSet.getString("Telefono");
		String email = resultSet.getString("Email");
		int domicilio = resultSet.getInt("idDomicilio");
		String nacimiento = resultSet.getString("Nacimiento");
		int tipo = resultSet.getInt("idTipoContacto");
		String usuarioLinkedin = resultSet.getString("UsuarioLinkedin");
		String mesNacimiento = resultSet.getString("MesNacimiento");
		return new PersonaDTO(id, nombre, apellido, tel, email, nacimiento, tipo, domicilio,usuarioLinkedin, mesNacimiento);
	}
}
