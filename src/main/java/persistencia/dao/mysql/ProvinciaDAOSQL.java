package persistencia.dao.mysql;

//import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;
import dto.ProvinciaDTO;

public class ProvinciaDAOSQL implements ProvinciaDAO
{

	private static final String findByIdPais = "SELECT * FROM provincia where idPais = ?";
		
	public List<ProvinciaDTO> findByIdPais(int idPais)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ProvinciaDTO> personas = new ArrayList<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(findByIdPais);
			statement.setInt(1, idPais);

			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getProvinciaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	
	public List<ProvinciaDTO> findProvinciaById(int idPais)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ProvinciaDTO> personas = new ArrayList<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(findByIdPais);
			statement.setInt(1, idPais);

			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getProvinciaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	
	private ProvinciaDTO getProvinciaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPronvincia");
		int idPais = resultSet.getInt("idPais");
		String nombre = resultSet.getString("nombre");
		return new ProvinciaDTO(id, idPais, nombre);
	}
}
