package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO
{
	private static final String insert = "INSERT INTO TipoContacto(idTipoContacto, NombreTipoContacto) VALUES(?, ?)";
	private static final String delete = "DELETE FROM TipoContacto WHERE idTipoContacto = ?";
	private static final String readall = "SELECT * FROM TipoContacto";
	private static final String findTipoContactoById = "SELECT * FROM TipoContacto where idTipoContacto = ? ";
		
	public boolean insert(TipoContactoDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdTipoContacto());
			statement.setString(2, persona.getNombre());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(TipoContactoDTO tipo_contato_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(tipo_contato_a_eliminar.getIdTipoContacto()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<TipoContactoDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoContactoDTO> TipoContacto = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				TipoContacto.add(getTipoContactoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return TipoContacto;
	}
	
	public TipoContactoDTO findTipoContactoById(int idTipoContacto)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		TipoContactoDTO TipoContacto = new TipoContactoDTO();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(findTipoContactoById);
			statement.setInt(1, idTipoContacto);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				TipoContacto = getTipoContactoDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return TipoContacto;
	}
	
	private TipoContactoDTO getTipoContactoDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idTipoContacto");
		String nombreTipoContacto = resultSet.getString("NombreTipoContacto");
		return new TipoContactoDTO(id, nombreTipoContacto);
	}
}
