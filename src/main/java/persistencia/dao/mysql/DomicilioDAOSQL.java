package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dto.DomicilioDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DomicilioDAO;

public class DomicilioDAOSQL implements DomicilioDAO
{
	private static final String insert = "INSERT INTO domicilio(calle, altura, piso, depto, idLocalidad, idProvincia, idPais) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM Domicilio WHERE idDomicilio = ?";
	private static final String update = "UPDATE domicilio SET calle = ?, altura = ?, piso = ?, depto = ?, idLocalidad = ?, idProvincia = ?, idPais = ? WHERE idDomicilio =?";
	private static final String readall = "SELECT * FROM domicilio";
	private static final String findByIdDomicilio = "SELECT * FROM domicilio where idDomicilio  = ?";
	
		
	public int insert(DomicilioDTO domicilio) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try {
			statement = conexion.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, domicilio.getCalle());
			statement.setString(2, domicilio.getAltura());
			statement.setString(3, domicilio.getPiso());
			statement.setString(4, domicilio.getDepto());
			statement.setInt(5, domicilio.getIdLocalidad());
			statement.setInt(6, domicilio.getIdProvincia());
			statement.setInt(7, domicilio.getIdPais());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()) {
					int generatedkey = rs.getInt(1);
					return generatedkey;

				}

			}
			return -1;

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return -1;
	}

	
	public boolean delete(int domicilio_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, domicilio_a_eliminar);
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	public boolean update(DomicilioDTO domicilio){
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try {
			PreparedStatement statement;
			statement = conexion.prepareStatement(update);
			statement.setString(1, domicilio.getCalle());
			statement.setString(2, domicilio.getAltura());
			statement.setString(3, domicilio.getPiso());
			statement.setString(4, domicilio.getDepto());
			statement.setInt(5, domicilio.getIdLocalidad());
			statement.setInt(6, domicilio.getIdProvincia());
			statement.setInt(7, domicilio.getIdPais());
			statement.setInt(8, domicilio.getIdDomicilio());
	
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				return true;
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}

	public List<DomicilioDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<DomicilioDTO> domicilios = new ArrayList<DomicilioDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				domicilios.add(getDomicilioDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return domicilios;
	}
	public DomicilioDTO findByIdDomicilio(int idDomicilio)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		DomicilioDTO domicilio = new DomicilioDTO();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(findByIdDomicilio);
			statement.setInt(1, idDomicilio);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				domicilio=(getDomicilioDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return domicilio;
	}
//	
	private DomicilioDTO getDomicilioDTO(ResultSet resultSet) throws SQLException
	{
		int idDomicilio = resultSet.getInt("idDomicilio");
		String calle = resultSet.getString("Calle");
		String altura = resultSet.getString("Altura");
		String piso = resultSet.getString("Piso");
		String depto = resultSet.getString("Depto");
		int idLocalidad = resultSet.getInt("idLocalidad");
		int idProvincia = resultSet.getInt("idProvincia");
		int idPais = resultSet.getInt("idPais");

		return new DomicilioDTO(idDomicilio, calle, altura, piso, depto, idLocalidad, idProvincia, idPais);
	}

}
