package persistencia.dao.interfaz;


public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	public ProvinciaDAO createProvinciaDAO();
	public LocalidadDAO createLocalidadDAO();
	public PaisDAO createPaisDAO();
	public DomicilioDAO createDomicilioDAO();
	public TipoContactoDAO createTipoContactoDAO();
}
