package persistencia.dao.interfaz;

import java.util.List;

//import dto.LocalidadDTO;
import dto.ProvinciaDTO;

public interface ProvinciaDAO 
{
	
	public List<ProvinciaDTO> findByIdPais(int idPais);
}
