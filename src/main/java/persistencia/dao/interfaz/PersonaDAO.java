package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

public interface PersonaDAO 
{
	
	public boolean insert(PersonaDTO domicilio);

	public boolean delete(PersonaDTO domicilio_a_eliminar);

	public boolean update(PersonaDTO domicilio_a_actualizar);
	
	public List<PersonaDTO> readAll();
}
