package persistencia.dao.interfaz;

import java.util.List;

//import dto.LocalidadDTO;
import dto.PaisDTO;
//import dto.ProvinciaDTO;

public interface PaisDAO 
{
	
	public List<PaisDTO> readAll();
}
