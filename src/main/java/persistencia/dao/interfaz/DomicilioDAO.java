package persistencia.dao.interfaz;

import dto.DomicilioDTO;

public interface DomicilioDAO 
{
	
	public int insert(DomicilioDTO persona);

	public boolean delete(int domiicilio_a_eliminar);

	public boolean update(DomicilioDTO domicilio_a_actualizar);

	public DomicilioDTO findByIdDomicilio(int idDomicilio);
}
