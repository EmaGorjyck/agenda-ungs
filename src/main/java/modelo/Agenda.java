package modelo;

import java.util.List;

import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private DomicilioDAO domicilio;	
	private ProvinciaDAO provincia;
	private LocalidadDAO localidad;
	private PaisDAO pais;
	private TipoContactoDAO tipoContacto;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.provincia = metodo_persistencia.createProvinciaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.pais = metodo_persistencia.createPaisDAO();
		this.domicilio = metodo_persistencia.createDomicilioDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
	}
	
	
	public int agregarDomicilio(DomicilioDTO nuevoDomicilio)
	{
		return this.domicilio.insert(nuevoDomicilio);
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}

	public void borrarDomicilio(int domicilio_a_eliminar){
		this.domicilio.delete(domicilio_a_eliminar);
	}
	
	public void actualizarPersona(PersonaDTO persona_a_actualizar){
		this.persona.update(persona_a_actualizar);
	}
	
	public void actualizarDomicilio(DomicilioDTO domicilio_a_actualizar){
		this.domicilio.update(domicilio_a_actualizar);
	}


	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	
	public List<TipoContactoDTO> obtenerTipoContactos()
	{
		return this.tipoContacto.readAll();		
	}
	
	
	public TipoContactoDTO obtenerTipoContactosById(int idTipoContacto)
	{
		return this.tipoContacto.findTipoContactoById(idTipoContacto);		
	}
	
	
	public List<ProvinciaDTO> obtenerProvincias(int idPais)
	{
		return this.provincia.findByIdPais(idPais);		
	}
	
	public List<PaisDTO> obtenerPaises()
	{
		return this.pais.readAll();		
	}
	
	public List<LocalidadDTO> obtenerLocalidades(int idProvincia)
	{
		return this.localidad.findByIdProvincia(idProvincia);		
	}
	
	public DomicilioDTO obtenerDomicilio(int idDomicilio) {
		return this.domicilio.findByIdDomicilio(idDomicilio);
	}
//	
//	public LocalidadDTO obtenerLocalidadById(int idLocalidad) {
//		return this.localidad.f
//	}
	
}
