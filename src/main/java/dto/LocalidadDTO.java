package dto;

public class LocalidadDTO {

	private int idLocalidad;
	private int idProvincia;
	private int idPais;
	private String nombre;
	
	
	public LocalidadDTO(int idLocalidad, String nombre, int idProvincia, int idPais) {
		super();
		this.idLocalidad = idLocalidad;
		this.idProvincia = idProvincia;
		this.idPais = idPais;
		this.nombre = nombre;
	}
	public int getIdLocalidad() {
		return idLocalidad;
	}
	public void setIdLocalidad(int idLocalidad) {
		this.idLocalidad = idLocalidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIdProvincia() {
		return idProvincia;
	}
	public void setIdProvincia(int idProvincia) {
		this.idProvincia = idProvincia;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	
	
	
}
