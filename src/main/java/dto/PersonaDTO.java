package dto;

public class PersonaDTO 
{
	
	private int idPersona;
	private String nombre;
	private String apellido;
	private String telefono;
	private String direccionEmail;
	private String fechaCumple;
	private int tipoContacto;
	private int idDomicilio;
	private String usuarioLinkedin;
	private String mesNacimiento;
	//private DomicilioDTO domicilioDto;
	


	public PersonaDTO(int idPersona, String nombre, String apellido, String telefono, String direccionEmail,
			String fechaCumple, int tipoContacto, int idDomicilio, String usuarioLinkedin, String mesNacimiento) {
		super();
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.direccionEmail = direccionEmail;
		this.fechaCumple = fechaCumple;
		this.tipoContacto = tipoContacto;
		this.idDomicilio = idDomicilio;
		this.usuarioLinkedin = usuarioLinkedin;
		this.mesNacimiento = mesNacimiento;
	}



	
	public int getIdPersona() 
	{
		return this.idPersona;
	}


	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getApellido(){
		return this.apellido;
	}

	public void setApellido(String apellido){
		this.apellido = apellido;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}

	public String getDireccionEmail(){
		return this.direccionEmail;
	}

	public void setDireccionEmail(String direccionEmail){
		this.direccionEmail = direccionEmail;
	}	

	public int getDomicilio(){
		return this.idDomicilio;
	}

	public void setDomicilio(int idDomicilio){
		this.idDomicilio = idDomicilio;
	}

	public String getFechaCumple(){
		return this.fechaCumple;
	}

	public void setFechaCumple(String fechaCumple){
		this.fechaCumple = fechaCumple;
	}

	public int getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(int tipoContacto) {
		this.tipoContacto = tipoContacto;
	}




	public int getIdDomicilio() {
		return idDomicilio;
	}




	public void setIdDomicilio(int idDomicilio) {
		this.idDomicilio = idDomicilio;
	}




	public String getUsuarioLinkedin() {
		return usuarioLinkedin;
	}




	public void setUsuarioLinkedin(String usuarioLinkedin) {
		this.usuarioLinkedin = usuarioLinkedin;
	}




	public String getMesNacimiento() {
		return mesNacimiento;
	}




	public void setMesNacimiento(String mesNacimiento) {
		this.mesNacimiento = mesNacimiento;
	}

	


}
