package dto;

public class ProvinciaDTO {
	
	private int idPronvincia;
	private int idPais;
	private String nombre;
	
	
	public ProvinciaDTO(int idPronvincia, int idPais, String nombre) {
		super();
		this.idPronvincia = idPronvincia;
		this.idPais = idPais;
		this.nombre = nombre;
	}
	public int getIdPais() {
		return idPais;
	}
	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
	public int getIdPronvincia() {
		return idPronvincia;
	}
	public void setIdPronvincia(int idPronvincia) {
		this.idPronvincia = idPronvincia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

}
