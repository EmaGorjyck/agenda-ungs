package dto;

public class ComboProvinciaDTO {
	
	private int idPronvincia;
	private String nombre;
	
	
	public ComboProvinciaDTO(int idPronvincia, String nombre) {
		super();
		this.idPronvincia = idPronvincia;
		this.nombre = nombre;
	}

	public int getIdPronvincia() {
		return idPronvincia;
	}
	
	public void setIdPronvincia(int idPronvincia) {
		this.idPronvincia = idPronvincia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString(){
		return this.nombre;
	}

}
