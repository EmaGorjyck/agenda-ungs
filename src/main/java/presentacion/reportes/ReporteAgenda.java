package presentacion.reportes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import modelo.Agenda;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ReporteAgenda
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Agenda agenda;
	private Logger log = Logger.getLogger(ReporteAgenda.class);
	//Recibe la lista de personas para armar el reporte
    public ReporteAgenda(List<Map> data, boolean tipo) throws IOException
    {	
		//true comun, false calendario
		if(tipo){
			//Hardcodeado
			Map<String, Object> parametersMap = new HashMap<String, Object>();
			parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));	
			BufferedImage image = ImageIO.read(new File("./resources/logoUngs.jpg"));
			parametersMap.put("logo", image );
			try		{
				this.reporte = (JasperReport) JRLoader.loadObjectFromFile("./resources/ReporteAgenda.jasper" );
				this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
						new JRBeanCollectionDataSource(data));
				log.info("Se cargó correctamente el reporte");
			}
			catch( JRException ex ) 
			{
				log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper", ex);
			}
		}
		else {
			//Hardcodeado
			Map<String, Object> parametersMap = new HashMap<String, Object>();
			parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));	
			try		{
				this.reporte = (JasperReport) JRLoader.loadObjectFromFile("./resources/ReporteAgendaCalendario.jasper");
				this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
						new JRBeanCollectionDataSource(data));
				log.info("Se cargó correctamente el reporte");
			}
			catch( JRException ex ) 
			{
				log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgendaCalendarios.Jasper", ex);
			}
		}
    	
    }       
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	