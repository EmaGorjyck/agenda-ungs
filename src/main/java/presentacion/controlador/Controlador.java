package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComboBox;

import dto.ComboLocalidadDTO;
import dto.ComboPaisDTO;
import dto.ComboProvinciaDTO;
import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;

public class Controlador implements ActionListener, MouseListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private List<ProvinciaDTO> provinciasEnTabla;
	private List<PaisDTO> paisesEnTabla;
	private List<TipoContactoDTO> tipoContactosEnCombo;
	private List<LocalidadDTO> localidadesEnTabla;
	private VentanaPersona ventanaPersona;
	private Agenda agenda;
	private int idEditar;
	private boolean Editar;
	private HashMap<String, Integer> meses = new HashMap<>();

	public Controlador(Vista vista, Agenda agenda) throws IOException {
		this.Editar = false;
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnEditar().addActionListener(e -> {
			try {
				editarPersona(e);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		this.vista.getBtnReporte().addActionListener(r -> {
			try {
				mostrarReporte(r);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		this.vista.getBtnReporteCalendario().addActionListener(r -> {
			try {
				mostrarReporteNacimiento(r);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> {
			if (CheckNull() || !isValidEmail()) {

				this.ventanaPersona.getAlerta().setVisible(true);
			} else {
				int idDomicilio = guardarDomicilio(p, Editar);
				guardarPersona(p, idDomicilio, Editar);
			}

		});
		// llenar los combo en la pantalla para agregar persona
		this.ventanaPersona.getComboBoxPais().addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent event) {
				// TODO Auto-generated method stub

				if (ventanaPersona.getComboPronvincias() != null
						&& ventanaPersona.getComboPronvincias().getItemCount() > 0) {
					ventanaPersona.getComboPronvincias().setSelectedIndex(-1);
				}

				if (event.getStateChange() == ItemEvent.SELECTED) {
					ComboPaisDTO item = (ComboPaisDTO) event.getItem();
					ventanaPersona.llenarComboPronvincias(agenda.obtenerProvincias(item.getIdPais()));
				}
			}
		});

		this.ventanaPersona.getComboPronvincias().addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent event) {
				// TODO Auto-generated method stub
				if (ventanaPersona.getComboLocalidades() != null
						&& ventanaPersona.getComboLocalidades().getItemCount() > 0) {
					ventanaPersona.getComboLocalidades().setSelectedIndex(-1);
				}
				if (event.getStateChange() == ItemEvent.SELECTED) {
					ComboProvinciaDTO item = (ComboProvinciaDTO) event.getItem();
					ventanaPersona.llenarComboLocalidades(agenda.obtenerLocalidades(item.getIdPronvincia()));
				}

			}
		});
		this.agenda = agenda;
	}

	private void ventanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.limpiarCampos();
		this.ventanaPersona.getAlerta().setVisible(false);
		this.ventanaPersona.mostrarVentana(true);
	}

	private int guardarDomicilio(ActionEvent p, boolean Editar) {
		int saveDomicilio = 0;
		// Domicilio
		String calle = this.ventanaPersona.getTxtCalle().getText();
		String altura = this.ventanaPersona.getTxtAltura().getText();
		String piso = this.ventanaPersona.getTxtPiso().getText();
		String depto = this.ventanaPersona.getTxtDepto().getText();

		int idLocalidad = ((ComboLocalidadDTO) this.ventanaPersona.getComboLocalidades().getSelectedItem())
				.getIdLocalidad();
		int idProvincia = ((ComboProvinciaDTO) ventanaPersona.getComboPronvincias().getSelectedItem())
				.getIdPronvincia();
		int idPais = ((ComboPaisDTO) ventanaPersona.getComboBoxPais().getSelectedItem()).getIdPais();

		if (Editar) {
			this.agenda.actualizarDomicilio(new DomicilioDTO(this.ventanaPersona.getIdDomicilio(), calle, altura, piso,
					depto, idLocalidad, idProvincia, idPais));
			saveDomicilio = this.ventanaPersona.getIdDomicilio();
		} else {
			DomicilioDTO nuevoDomicilio = new DomicilioDTO(calle, altura, piso, depto, idLocalidad, idProvincia,
					idPais);
			saveDomicilio = this.agenda.agregarDomicilio(nuevoDomicilio);
		}

		return saveDomicilio;
	}

	private void guardarPersona(ActionEvent p, int idDomicilio, boolean editar) {
		// Datos contacto
		System.out.println(Editar);
		System.out.println(editar);
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String apellido = this.ventanaPersona.getTxtApellido().getText();

		int idTipoContacto = ((TipoContactoDTO) ventanaPersona.getComboTipoContacto().getSelectedItem())
				.getIdTipoContacto();
		String tel = this.ventanaPersona.getTxtTelefono().getText();
		String email = this.ventanaPersona.getTxtEmail().getText();
		String usuarioLinkedin = this.ventanaPersona.getTxtUsuarioLinkedin().getText();
		java.util.Date dateNacimiento =  this.ventanaPersona.getTxtNacimiento().getDate();
		java.text.SimpleDateFormat formatDate = new java.text.SimpleDateFormat("dd/MM/yyyy");
		String nacimiento = formatDate.format(dateNacimiento);
		String[] salida = nacimiento.split("/");
		String mesTexto = mes(salida[01]);
		System.out.println(mesTexto);
		String mesNacimiento = mesTexto;
		
		if (editar) {
			PersonaDTO nuevaPersona = new PersonaDTO(this.idEditar, nombre, apellido, tel, email,
					nacimiento, idTipoContacto, idDomicilio,  usuarioLinkedin, mesNacimiento);
			this.agenda.actualizarPersona(nuevaPersona);
			this.Editar = false;
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		} else {
			PersonaDTO nuevaPersona = new PersonaDTO(this.agenda.obtenerPersonas().size(), nombre, apellido, tel, email,
					nacimiento, idTipoContacto, idDomicilio,  usuarioLinkedin, mesNacimiento);
			this.agenda.agregarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.limpiarCampos();
			this.ventanaPersona.cerrar();
		}
	}

	public void guardarContacto(ActionEvent g) {

	}

	private void mostrarReporte(ActionEvent r) throws IOException {
		//armar datos
		final List<Map> data = new ArrayList<Map>();
		for (PersonaDTO  persona : agenda.obtenerPersonas()) {
			HashMap<String, Object> rowMap = new HashMap<String, Object>();
			DomicilioDTO domicilio= this.agenda.obtenerDomicilio(persona.getDomicilio());
			rowMap.put("nombre", persona.getNombre() + " "+ persona.getApellido());
			rowMap.put("telefono", String.valueOf(persona.getTelefono()));  
			rowMap.put("email", persona.getDireccionEmail());
			rowMap.put("fechaCumple", String.valueOf(persona.getFechaCumple()));
			String tipo = setSelectedValueTipo(this.ventanaPersona.getComboTipoContacto(), persona.getTipoContacto());
			rowMap.put("tipo", tipo);
			rowMap.put("usuarioLinkedin", persona.getUsuarioLinkedin());
			rowMap.put("mesNacimiento", persona.getMesNacimiento());
			
			//domicilio
			rowMap.put("calle", domicilio.getCalle());
			rowMap.put("altura", String.valueOf(domicilio.getAltura()));
			rowMap.put("piso", String.valueOf(domicilio.getPiso()));
			
			rowMap.put("depto", String.valueOf(domicilio.getDepto()));
			String pais = setSelectedValuePais(this.ventanaPersona.getComboBoxPais(), domicilio.getIdPais());
			rowMap.put("pais", pais);
			String provincia = setSelectedValueProvincia(this.ventanaPersona.getComboPronvincias(), domicilio.getIdProvincia());
			rowMap.put("provincia", provincia);
			String localidad = setSelectedValueLocalidad(this.ventanaPersona.getComboLocalidades(), domicilio.getIdLocalidad());
			rowMap.put("localidad", localidad);
			data.add(rowMap);
		}
		ReporteAgenda reporte = new ReporteAgenda(data, true);
		
		reporte.mostrar();
	}
	private void mostrarReporteNacimiento(ActionEvent r) throws IOException{
		meses = new HashMap<String, Integer>();
		final List<Map> data = new ArrayList<Map>();
		for(PersonaDTO persona : agenda.obtenerPersonas()){
			System.out.println(persona.getMesNacimiento());
			this.mesesCount(persona.getMesNacimiento());

		}
		System.out.println(meses);
		for(String mes : meses.keySet()){
			HashMap<String, Object> rowMap = new HashMap<String, Object>();
			rowMap.put("Meses", mes);
			rowMap.put("cantmeses", meses.get(mes));
			data.add(rowMap);
		}
		ReporteAgenda reporteNacimiento = new ReporteAgenda(data, false);
		reporteNacimiento.mostrar();
	}
	private void editarPersona(ActionEvent e) throws ParseException {
		// carga los campos con los datos del contacto a editar
		CargarCampos(this.vista.getTablaPersonas().getSelectedRows());
		this.ventanaPersona.getAlerta().setVisible(false);
		this.Editar = true;
		System.out.println(Editar);
		this.ventanaPersona.mostrarVentana(true);
	}

	public void cargarProvincias(ActionEvent p) {
		this.provinciasEnTabla = agenda.obtenerProvincias(this.ventanaPersona.getComboPronvincias().getSelectedIndex());
		this.ventanaPersona.llenarComboPronvincias(provinciasEnTabla);
	}

	public void cargarLocalidades(ActionEvent p) {
		this.localidadesEnTabla = agenda
				.obtenerLocalidades(this.ventanaPersona.getComboPronvincias().getSelectedIndex());
		this.ventanaPersona.llenarComboPronvincias(provinciasEnTabla);
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			this.agenda.borrarDomicilio(this.personasEnTabla.get(fila).getDomicilio());
		}

		this.refrescarTabla();
	}

	public static String setSelectedValueLocalidad(JComboBox comboBox, int value) {
		ComboLocalidadDTO item;
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			item = (ComboLocalidadDTO) comboBox.getItemAt(i);
			if (item.getIdLocalidad() == value) {
				comboBox.setSelectedIndex(i);
				return comboBox.getItemAt(i).toString();
			}
		}
		return "";
	}

	public static String setSelectedValueProvincia(JComboBox comboBox, int value) {
		ComboProvinciaDTO item;
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			item = (ComboProvinciaDTO) comboBox.getItemAt(i);
			if (item.getIdPronvincia() == value) {
				System.out.println("Provincia" + comboBox.getItemAt(i));
				comboBox.setSelectedIndex(i);
				return comboBox.getItemAt(i).toString();
			}
		}
		return "";
	}

	public static String setSelectedValuePais(JComboBox comboBox, int value) {
		ComboPaisDTO item;
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			item = (ComboPaisDTO) comboBox.getItemAt(i);
			if (item.getIdPais() == value) {
				comboBox.setSelectedIndex(i);
				return comboBox.getItemAt(i).toString();
			}
		}
		return "";
	}

	public static String setSelectedValueTipo(JComboBox comboBox, int value) {
		TipoContactoDTO item;
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			item = (TipoContactoDTO) comboBox.getItemAt(i);
			if (item.getIdTipoContacto() == value) {
				comboBox.setSelectedIndex(i);
				return comboBox.getItemAt(i).toString();
			}
		}
		return "";
	}

	public void cancelarEdicion(ActionEvent c) {
		System.out.println("cancelar Edicion");
		this.ventanaPersona.cerrar();
	}

	public void inicializar() {
		this.refrescarTabla();
		this.vista.show();
		this.vista.getTablaPersonas().addMouseListener(this);
	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.paisesEnTabla = agenda.obtenerPaises();
		this.tipoContactosEnCombo = agenda.obtenerTipoContactos();
		this.llenarTabla(this.personasEnTabla);
		this.ventanaPersona.llenarComboPaises(paisesEnTabla);
		this.ventanaPersona.llenarComboTipoContactos(this.tipoContactosEnCombo);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.vista.getModelPersonas().setRowCount(0); // Para vaciar la tabla
		this.vista.getModelPersonas().setColumnCount(0);
		this.vista.getModelPersonas().setColumnIdentifiers(this.vista.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla) {
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String apellido = p.getApellido();
			String email = p.getDireccionEmail();
			String nacimiento = p.getFechaCumple();
			TipoContactoDTO tipoContacto = this.agenda.obtenerTipoContactosById(p.getTipoContacto());
			String tipo = tipoContacto.getNombre();
			Object[] fila = { nombre, apellido, tel, email, nacimiento, tipo };
			this.vista.getModelPersonas().addRow(fila);
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// logica para el doble click
		if (e.getClickCount() == 2) {
			try {
				CargarCampos(this.vista.getTablaPersonas().getSelectedRows());
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			this.ventanaPersona.mostrarVentana(false);
		}
	}

	private void CargarCampos(int[] filasSeleccionadas) throws ParseException {
		for (int fila : filasSeleccionadas) {
			this.idEditar = this.personasEnTabla.get(fila).getIdPersona();
			// buscamos datos del domicilio
			DomicilioDTO domicilioPersona = this.agenda.obtenerDomicilio(this.personasEnTabla.get(fila).getDomicilio());

			this.ventanaPersona.getTxtNombre().setText(this.personasEnTabla.get(fila).getNombre());
			this.ventanaPersona.getTxtApellido().setText(this.personasEnTabla.get(fila).getApellido());
			this.ventanaPersona.getTxtTelefono().setText(this.personasEnTabla.get(fila).getTelefono());
			this.ventanaPersona.getTxtEmail().setText(this.personasEnTabla.get(fila).getDireccionEmail());
			this.ventanaPersona.setIdDomicilio(this.personasEnTabla.get(fila).getDomicilio());
			this.ventanaPersona.getTxtUsuarioLinkedin().setText(this.personasEnTabla.get(fila).getUsuarioLinkedin());
			this.ventanaPersona.getTxtMesNacimiento().setText(this.personasEnTabla.get(fila).getMesNacimiento());
			
			
			String fechaString=this.personasEnTabla.get(fila).getFechaCumple();  
		    Date fechaDate=new SimpleDateFormat("dd/MM/yyyy").parse(fechaString);  
			
			this.ventanaPersona.getTxtNacimiento().setDate(fechaDate);

			TipoContactoDTO tipoContacto = this.agenda
					.obtenerTipoContactosById(this.personasEnTabla.get(fila).getTipoContacto());
			setSelectedValueTipo(this.ventanaPersona.getComboTipoContacto(), tipoContacto.getIdTipoContacto());

			// datos de domicilio
			this.ventanaPersona.getTxtAltura().setText(domicilioPersona.getAltura());
			this.ventanaPersona.getTxtCalle().setText(domicilioPersona.getCalle());
			this.ventanaPersona.getTxtDepto().setText(domicilioPersona.getDepto());
			this.ventanaPersona.getTxtPiso().setText(domicilioPersona.getPiso());
		

			setSelectedValuePais(this.ventanaPersona.getComboBoxPais(), domicilioPersona.getIdPais());
			setSelectedValueProvincia(this.ventanaPersona.getComboPronvincias(), domicilioPersona.getIdProvincia());
			setSelectedValueLocalidad(this.ventanaPersona.getComboLocalidades(), domicilioPersona.getIdLocalidad());
		}
	}

	private boolean CheckNull() {
		boolean isNull = true;
		if (!isBlank(this.ventanaPersona.getTxtNombre().getText()) 
				&& !isBlank(this.ventanaPersona.getTxtApellido().getText())
				&& !isBlank(this.ventanaPersona.getTxtEmail().getText()) && this.ventanaPersona.getTxtNacimiento().getDate() != null
				&& !isBlank(this.ventanaPersona.getTxtTelefono().getText())  && !isBlank(this.ventanaPersona.getTxtAltura().getText())
				&& !isBlank(this.ventanaPersona.getTxtCalle().getText())  && !isBlank(this.ventanaPersona.getTxtDepto().getText())
				&& !isBlank(this.ventanaPersona.getTxtPiso().getText()) 
				&& this.ventanaPersona.getComboBoxPais().getSelectedItem() != null
				&& this.ventanaPersona.getComboTipoContacto().getSelectedItem() != null
				&& this.ventanaPersona.getComboPronvincias().getSelectedItem() != null
				&& this.ventanaPersona.getComboLocalidades().getSelectedItem() != null) {
			isNull = false;
		}
		
		if(isNull) {
			this.ventanaPersona.getAlerta().setText("DEBE COMPLETAR TODOS LOS CAMPOS");
		}
		
		return isNull;
	}
	
	private boolean isBlank(String valor) {
		if(valor == null || valor.replaceAll("\\s","").equals("")) {
			return true;
		}
		return false;
		
	}
	
	private HashMap<String, Integer> mesesCount(String mes){
		if(meses.isEmpty()){
			meses.put(mes, 1);
		}
		else {
			for(String mesMap : meses.keySet()){
				if(mesMap.equals(mes)){
					meses.put(mes, meses.get(mes)+1);
					return meses;
				}
			}
			meses.put(mes, 1);
		}
		return meses;
	}
	private boolean isValidEmail() {
		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
		 
		Pattern pattern = Pattern.compile(regex);
		 
		Matcher matcher = pattern.matcher(this.ventanaPersona.getTxtEmail().getText());
		boolean isValid =  matcher.matches();
		if(!isValid) {
			this.ventanaPersona.getAlerta().setText("El email ingresado no es un formato valido");
		}
		return isValid;
		  
	}

	private String mes(String numero){
		String mes = "";
		switch (numero){
			case "01":{
				mes = "1-Enero";
				break;
			}
			case "02":{
				mes = "2-Febrero";
				break;
			}
			case "03":{
				mes = "3-Marzo";
				break;
			}
			case "04":{
				mes = "4-Abril";
				break;
			}
			case "05":{
				mes = "5-Mayo";
				break;
			}
			case "06":{
				mes = "6-Junio";
				break;
			}
			case "07":{
				mes = "7-Julio";
				break;
			}
			case "08":{
				mes = "8-Agosto";
				break;
			}
			case "09":{
				mes = "9-Septiembre";
				break;
			}
			case "10":{
				mes = "10-Octubre";
				break;
			}
			case "11":{
				mes = "11-Noviembre";
				break;
			}
			case "12":{
				mes = "12-Diciembre";
				break;
			}
		}
		return mes;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
