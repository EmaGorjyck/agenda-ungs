package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import modelo.Agenda;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.vista.Login;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;

public class LoginController implements ActionListener, MouseListener {
	private Vista vista;
	private Login login;

	public LoginController() throws IOException {
		this.login = Login.getInstance();
		this.login.getBtnLogin().addActionListener(a -> {
			try {
				loginPersona(a);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		// llenar los combo en la pantalla para agregar persona
	}

	private void loginPersona(ActionEvent a) throws IOException {
//		this.vista.limpiarCampos();
//		this.vista.getAlerta().setVisible(false);
		System.out.print("ingresando vaolors!");
		String nombre = this.login.getTextNombreUsuario().getText();
		String password = this.login.getTextPassword().getText();
		System.out.print("nombre"+ nombre + "pass "+ password);
		
		if("gus".equals(nombre) && "gen".equals(password)) {
//			System.out.print("nombre"+ nombre + "pass "+ password);
			Vista vista = new Vista();
			Agenda modelo = new Agenda(new DAOSQLFactory());
			Controlador controlador = new Controlador(vista, modelo);
			controlador.inicializar();
//			this.vista.mostrarVentana();
			this.login.cerrar();
		}
	
	}


	public void inicializar() {

		this.login.show();
	}

	private void refrescarTabla() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	private boolean isBlank(String valor) {
		if(valor == null || valor.replaceAll("\\s","").equals("")) {
			return true;
		}
		return false;
		
	}
//	
//	private boolean isValidEmail() {
//		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
//		 
//		Pattern pattern = Pattern.compile(regex);
//		 
//		Matcher matcher = pattern.matcher(this.ventanaPersona.getTxtEmail().getText());
//		boolean isValid =  matcher.matches();
//		if(!isValid) {
//			this.ventanaPersona.getAlerta().setText("El email ingresado no es un formato valido");
//		}
//		return isValid;
//		  
//	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
