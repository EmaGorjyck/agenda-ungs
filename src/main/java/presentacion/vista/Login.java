package presentacion.vista;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.imageio.ImageIO;
import javax.swing.JButton;

import persistencia.conexion.Conexion;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;

public class Login
{
	private JFrame frame;
	private JButton btnLogin;
	private DefaultTableModel modelPersonas;
	private static Login INSTANCE;

	private  String[] nombreColumnas = {"Nombre","Apellido","Telefono","Email", "Fecha de Nacimiento", "Tipo"};
	private JTextField textNombreUsuario;
	private JTextField textPassword;

	
	public static Login getInstance() throws IOException
	{
		if(INSTANCE == null)
		{
			INSTANCE = new Login(); 	
			return new Login();
		}
		else
			return INSTANCE;
	}

	
	
	public Login() throws IOException 
	{
		super();
		initialize();
	}


	private void initialize() throws IOException 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 330, 245);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setIconImage(ImageIO.read(new File("./resources/iconWin.jpg")));
		frame.setTitle("Agenda UNGS");
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 314, 214);
		panel.setBackground(new Color(188, 212, 255));
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		//tabla no editable con 2 click
		modelPersonas = new DefaultTableModel(null,nombreColumnas){
			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		
		btnLogin = new JButton("Ingresar");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnLogin.setBounds(77, 143, 173, 23);
		panel.add(btnLogin);
		
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 14));
		lblNewLabel.setBounds(41, 43, 77, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Contraseña");
		lblNewLabel_1.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 14));
		lblNewLabel_1.setBounds(41, 68, 89, 23);
		panel.add(lblNewLabel_1);
		
		textNombreUsuario = new JTextField();
		textNombreUsuario.setBounds(128, 42, 138, 20);
		panel.add(textNombreUsuario);
		textNombreUsuario.setColumns(10);
		
		textPassword = new JTextField();
		textPassword.setBounds(128, 71, 138, 20);
		panel.add(textPassword);
		textPassword.setColumns(10);
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	

	public void cerrar()
	{
		this.frame.dispose();
	}

	public JButton getBtnLogin() {
		return btnLogin;
	}


	public void setBtnLogin(JButton btnLogin) {
		this.btnLogin = btnLogin;
	}


	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}


	public JTextField getTextNombreUsuario() {
		return textNombreUsuario;
	}


	public void setTextNombreUsuario(JTextField textNombreUsuario) {
		this.textNombreUsuario = textNombreUsuario;
	}


	public JTextField getTextPassword() {
		return textPassword;
	}


	public void setTextPassword(JTextField textPassword) {
		this.textPassword = textPassword;
	}
	
	
}
