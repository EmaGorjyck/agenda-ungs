package presentacion.vista;

import java.awt.Color;
import java.awt.Component;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import com.toedter.calendar.JDateChooser;

import dto.ComboLocalidadDTO;
import dto.ComboPaisDTO;
import dto.ComboProvinciaDTO;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;

import java.awt.Font;
import java.io.File;
import java.io.IOException;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtTelefono;

	private JTextField txtEmail;
	private JTextField txtTipo;
	private JComboBox<String> comboPais;
	private JComboBox<ComboProvinciaDTO> comboBoxProvincia;
	 
	 Vector modelProvincias = new Vector();
	 Vector modelPaises = new Vector();
	 Vector modelTipoContactos = new Vector();
	 Vector modelLocalidades = new Vector();
	
	private JButton btnAgregarPersona;
	private static VentanaPersona INSTANCE;
	private JComboBox comboBoxPais;
	private JLabel lblLocalidad;
	private JComboBox<ComboLocalidadDTO> comboBoxLocalidad;
	JComboBox<TipoContactoDTO> comboTipoContacto;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblAlerta;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JLabel lblNewLabel_1_1;
	private JLabel lblNewLabel_1_2;
	private JLabel lblNewLabel_1_3;
	private JTextField txtDepto;
	private JTextField txtPiso;
	private JDateChooser txtNacimiento = new com.toedter.calendar.JDateChooser();
	private int idDomicilio;
	private JPanel panel = new JPanel();
	private JTextField txtUsuarioLinkedin;
	private JTextField txtMesNacimiento;
	
	public static VentanaPersona getInstance() throws IOException
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	private VentanaPersona() throws IOException 
	{
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		super.setIconImage(ImageIO.read(new File("./resources/iconWin.jpg")));
		super.setTitle("CONTACTO");
		
	
		panel.setBounds(10, 11, 764, 500);
		panel.setBackground(new Color(188, 212, 255));
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		
		//LABELS
		JLabel lblNombreYApellido = new JLabel("Nombre");
		lblNombreYApellido.setBounds(50, 10, 113, 14);
		panel.add(lblNombreYApellido);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(50, 40, 113, 14);
		panel.add(lblApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(50, 70, 113, 14);
		panel.add(lblTelfono);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(50, 100, 113, 14);
		panel.add(lblEmail);

		JLabel lblNacimiento = new JLabel("Nacimiento");
		lblNacimiento.setBounds(50, 130, 113, 14);
		panel.add(lblNacimiento);

		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(50, 160, 113, 14);
		panel.add(lblTipo);
		
		this.lblAlerta = new JLabel();
		lblAlerta.setBounds(250, 330, 300, 100);
		panel.add(lblAlerta);
		
		//Campos Inputs
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(133, 38, 164, 20);
		panel.add(txtApellido);
		txtApellido.setColumns(10);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 68, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(133, 98, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		txtEmail.setColumns(10);
		
		//agregar lista de provincias
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(308, 272, 49, 14);
		panel.add(lblProvincia);

		comboBoxProvincia = new JComboBox(this.modelProvincias);
		comboBoxProvincia.setRenderer( new ItemRenderer() );
	
		comboBoxProvincia.setBounds(386, 269, 164, 20);
		
		panel.add(comboBoxProvincia);
		
		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(308, 241, 43, 14);
		panel.add(lblPais);

		comboBoxPais = new JComboBox<ComboPaisDTO>(this.modelPaises);
		comboBoxPais.setBounds(386, 238, 164, 20);
		comboBoxPais.setRenderer( new ItemRenderer());

		panel.add(comboBoxPais);
		
		
		lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(308, 300, 57, 14);
		panel.add(lblLocalidad);
		
		comboBoxLocalidad = new JComboBox(this.modelLocalidades);
		comboBoxLocalidad.setBounds(386, 300, 164, 20);
		comboBoxLocalidad.setRenderer( new ItemRenderer() );
		panel.add(comboBoxLocalidad);
	
		
		//boton agregar
		btnAgregarPersona = new JButton("Guardar");
		btnAgregarPersona.setBounds(308, 350, 109, 23);
		panel.add(btnAgregarPersona);
		
		lblNewLabel = new JLabel("Domicilio");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(50, 219, 89, 14);
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Calle ");
		lblNewLabel_1.setBounds(50, 244, 57, 14);
		panel.add(lblNewLabel_1);
		
		txtCalle = new JTextField();
		txtCalle.setColumns(10);
		txtCalle.setBounds(133, 241, 164, 20);
		panel.add(txtCalle);
		
		txtAltura = new JTextField();
		txtAltura.setColumns(10);
		txtAltura.setBounds(133, 269, 164, 20);
		panel.add(txtAltura);
		
		lblNewLabel_1_1 = new JLabel("Altura");
		lblNewLabel_1_1.setBounds(50, 272, 57, 14);
		panel.add(lblNewLabel_1_1);
		
		lblNewLabel_1_2 = new JLabel("Piso");
		lblNewLabel_1_2.setBounds(50, 300, 57, 14);
		panel.add(lblNewLabel_1_2);
		
		lblNewLabel_1_3 = new JLabel("Depto");
		lblNewLabel_1_3.setBounds(50, 328, 57, 14);
		panel.add(lblNewLabel_1_3);
		
		txtDepto = new JTextField();
		txtDepto.setColumns(10);
		txtDepto.setBounds(133, 325, 164, 20);
		panel.add(txtDepto);
		
		txtPiso = new JTextField();
		txtPiso.setColumns(10);
		txtPiso.setBounds(133, 297, 164, 20);
		panel.add(txtPiso);
		
		comboTipoContacto = new JComboBox<TipoContactoDTO>(this.modelTipoContactos);
		comboTipoContacto.setBounds(133, 155, 164, 20);
		comboTipoContacto.setRenderer( new ItemRenderer() );;
		panel.add(comboTipoContacto);
		
	
		txtNacimiento.setLocation(133, 125);
		txtNacimiento.setSize(164, 20);
		txtNacimiento.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		txtNacimiento.setDateFormatString("dd/MM/yyyy");
		panel.add(txtNacimiento);
		
		JLabel lblDatosAdicionales = new JLabel("Datos Adicionales");
		lblDatosAdicionales.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDatosAdicionales.setBounds(326, 10, 113, 14);
		panel.add(lblDatosAdicionales);
		
		JLabel lblUsuarioLinkedin = new JLabel("Usuario Linkedin");
		lblUsuarioLinkedin.setBounds(326, 40, 113, 14);
		panel.add(lblUsuarioLinkedin);
		
		JLabel lblMesNacimiento = new JLabel("Mes Nacimiento");
		lblMesNacimiento.setBounds(326, 70, 113, 14);
		panel.add(lblMesNacimiento);
		
		txtUsuarioLinkedin = new JTextField();
		txtUsuarioLinkedin.setColumns(10);
		txtUsuarioLinkedin.setBounds(433, 37, 164, 20);
		panel.add(txtUsuarioLinkedin);
		
		txtMesNacimiento = new JTextField();
		txtMesNacimiento.setColumns(10);
		txtMesNacimiento.setBounds(433, 67, 164, 20);
		panel.add(txtMesNacimiento);
		txtMesNacimiento.setEditable(false);
		
		this.setVisible(false);
	}
	
    class ItemRenderer extends BasicComboBoxRenderer
    {
        public Component getListCellRendererComponent(
            JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus)
        {
            super.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);
 
            if (value != null)
            {
                if (value instanceof ComboProvinciaDTO) {
                	ComboProvinciaDTO item = (ComboProvinciaDTO)value;
                	  setText( item.getNombre().toUpperCase() );
                }
                if (value instanceof ComboPaisDTO) {
                	ComboPaisDTO item = (ComboPaisDTO)value;
                	  setText( item.getNombre().toUpperCase() );
                }
                if (value instanceof ComboLocalidadDTO) {
                	ComboLocalidadDTO item = (ComboLocalidadDTO)value;
                	  setText( item.getNombre().toUpperCase() );
                }
                if (value instanceof TipoContactoDTO) {
                	TipoContactoDTO item = (TipoContactoDTO)value;
                	  setText( item.getNombre().toUpperCase() );
                }
            }
            return this;
        }
    }
	
	public void mostrarVentana(boolean editarVentana)
	{
		this.setVisible(true);
		if(editarVentana) {
			System.out.print(" editar");
			for (Component c : this.panel.getComponents()) {
				   if (c instanceof JTextField || c instanceof JComboBox<?> || c instanceof JButton || c instanceof JDateChooser) {
				      c.setEnabled(true);
				   }
				}
		}else {
			System.out.print("no editar");
			for (Component c : this.panel.getComponents()) {
				   if (c instanceof JTextField || c instanceof JComboBox<?> || c instanceof JButton || c instanceof JDateChooser) {
				      c.setEnabled(false);
				   }
			}
		}
		
	}
	
	public void llenarComboPronvincias(List<ProvinciaDTO> provinciasEnTabla) {
	
		this.modelProvincias.clear();
		this.modelProvincias.add(new ComboProvinciaDTO(-1, "SELECT"));
		for (ProvinciaDTO p : provinciasEnTabla)
		{
			this.modelProvincias.add( new ComboProvinciaDTO(p.getIdPronvincia(), p.getNombre()));
		}
//		this.comboBoxProvincia= new JComboBox(this.modelProvincias);
	
	}
	
	public void llenarComboLocalidades(List<LocalidadDTO> localidadesEnTabla) {
		this.modelLocalidades.clear();
		this.modelLocalidades.add(new ComboLocalidadDTO(-1, "SELECT"));
		for (LocalidadDTO p : localidadesEnTabla)
		{
			this.modelLocalidades.add( new ComboLocalidadDTO(p.getIdLocalidad(), p.getNombre()));
		}
//		this.comboBoxLocalidad= new JComboBox(this.modelLocalidades);
	
		
	}
	
	public void llenarComboPaises(List<PaisDTO> paisesEnTabla) {
		this.modelPaises.clear();
		this.modelPaises.add(new ComboPaisDTO(-1, "SELECT"));
		for (PaisDTO p : paisesEnTabla)
		{
			this.modelPaises.add( new ComboPaisDTO(p.getIdPais(), p.getNombre()));
		}
//		this.comboBoxPais= new JComboBox(this.modelPaises);
	
		
	}
	
	public void llenarComboTipoContactos(List<TipoContactoDTO> tiposContactosEnTabla) {
		this.modelTipoContactos.clear();
		this.modelTipoContactos.add(new TipoContactoDTO(-1, "SELECT"));
		for (TipoContactoDTO p : tiposContactosEnTabla)
		{
			this.modelTipoContactos.add( new TipoContactoDTO(p.getIdTipoContacto(), p.getNombre()));
		}
	
		
	}
	
	public JLabel getAlerta (){
		return this.lblAlerta;
	} 
	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}


	public void setTxtNacimiento(JDateChooser txtNacimiento) {
		this.txtNacimiento = txtNacimiento;
	}

	public void setTxtTipo(JTextField txtTipo) {
		this.txtTipo = txtTipo;
	}

	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtApellido(){
		return txtApellido;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	public JTextField getTxtEmail(){
		return txtEmail;
	}

	public JDateChooser getTxtNacimiento() {
		return txtNacimiento;
	}

	public JTextField getTxtTipo(){
		return txtTipo;
	}
	

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public void setTxtPiso(JTextField txtPiso) {
		this.txtPiso = txtPiso;
	}

	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public void setTxtCalle(JTextField txtCalle) {
		this.txtCalle = txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public void setTxtAltura(JTextField txtAltura) {
		this.txtAltura = txtAltura;
	}

	public JTextField getTxtDepto() {
		return txtDepto;
	}

	public void setTxtDepto(JTextField txtDepto) {
		this.txtDepto = txtDepto;
	}

	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	public JComboBox<ComboLocalidadDTO> getComboLocalidades() {
		return comboBoxLocalidad;
	}

	public void setComboLocalidades(JComboBox<ComboLocalidadDTO> comboLocalidades) {
		this.comboBoxLocalidad = comboLocalidades;
	}


	public JButton getBtnAgregarPersona() {
		return btnAgregarPersona;
	}

	public void setBtnAgregarPersona(JButton btnAgregarPersona) {
		this.btnAgregarPersona = btnAgregarPersona;
	}

	public JComboBox getComboPronvincias() {
		return comboBoxProvincia;
	}

	public void setComboPronvincias(JComboBox comboPronvincias) {
		this.comboBoxProvincia = comboPronvincias;
	}

	
	public JComboBox getComboBoxPais() {
		return comboBoxPais;
	}

	public void setComboBoxPais(JComboBox comboBoxPais) {
		this.comboBoxPais = comboBoxPais;
	}
	

	public Vector getModelProvincias() {
		return modelProvincias;
	}

	public void setModelProvincias(Vector modelProvincias) {
		this.modelProvincias = modelProvincias;
	}

	public Vector getModelPaises() {
		return modelPaises;
	}

	public void setModelPaises(Vector modelPaises) {
		this.modelPaises = modelPaises;
	}

	public Vector getModelLocalidades() {
		return modelLocalidades;
	}

	public void setModelLocalidades(Vector modelLocalidades) {
		this.modelLocalidades = modelLocalidades;
	}

	public int getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(int idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	
	
	public JComboBox<TipoContactoDTO> getComboTipoContacto() {
		return comboTipoContacto;
	}

	public void setComboTipoContacto(JComboBox<TipoContactoDTO> comboTipoContacto) {
		this.comboTipoContacto = comboTipoContacto;
	}
	
	

	public JTextField getTxtUsuarioLinkedin() {
		return txtUsuarioLinkedin;
	}

	public void setTxtUsuarioLinkedin(JTextField txtUsuarioLinkedin) {
		this.txtUsuarioLinkedin = txtUsuarioLinkedin;
	}

	public JTextField getTxtMesNacimiento() {
		return txtMesNacimiento;
	}

	public void setTxtMesNacimiento(JTextField txtMesNacimiento) {
		this.txtMesNacimiento = txtMesNacimiento;
	}

	public void limpiarCampos(){
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtApellido.setText(null);
		this.txtEmail.setText(null);
		this.txtNacimiento.setDate(new Date());
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.txtPiso.setText(null);
		this.txtDepto.setText(null);
		this.txtMesNacimiento.setText(null);
		this.txtUsuarioLinkedin.setText(null);
		this.comboBoxPais.setSelectedIndex(-1);
		this.comboBoxProvincia.setSelectedIndex(-1);
		this.comboBoxLocalidad.setSelectedIndex(-1);
		this.comboTipoContacto.setSelectedIndex(-1);
	}
	public void cerrar()
	{
		this.limpiarCampos();
		this.dispose();
	}
}

