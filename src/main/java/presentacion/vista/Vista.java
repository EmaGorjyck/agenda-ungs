package presentacion.vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;

import persistencia.conexion.Conexion;

public class Vista extends JFrame
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnEditar;
	private JButton btnReporteCalendario;
	private DefaultTableModel modelPersonas;

	private  String[] nombreColumnas = {"Nombre","Apellido","Telefono","Email", "Fecha de Nacimiento", "Tipo"};

	public Vista() throws IOException 
	{
		super();
		initialize();
	}

	
	public void mostrarVentana() throws IOException
	{
		this.setVisible(true);
	
		
	}

	private void initialize() throws IOException 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 850, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setIconImage(ImageIO.read(new File("./resources/iconWin.jpg")));
		frame.setTitle("Agenda UNGS");
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 834, 262);
		panel.setBackground(new Color(188, 212, 255));
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 814, 182);
		panel.add(spPersonas);
		//tabla no editable con 2 click
		modelPersonas = new DefaultTableModel(null,nombreColumnas){
			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		tablaPersonas = new JTable(modelPersonas);
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		tablaPersonas.removeColumn(tablaPersonas.getColumnModel().getColumn(4));// idDomicilio hide field
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(50, 228, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(209, 228, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(408, 228, 89, 23);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(507, 228, 89, 23);
		panel.add(btnReporte);

		btnReporteCalendario = new JButton("Reporte Nacimiento");
		btnReporteCalendario.setBounds(607, 228, 200, 23);
		panel.add(btnReporteCalendario);
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnEditar()
	{
		return btnEditar;
	}
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}

	public JButton getBtnReporteCalendario(){
		return btnReporteCalendario;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}


}
